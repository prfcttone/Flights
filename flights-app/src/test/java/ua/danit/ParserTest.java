package ua.danit;

import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;
import ua.danit.framework.Parser;
import ua.danit.framework.invocations.Parameter;
import ua.danit.framework.utils.Reflections;

import javax.ws.rs.QueryParam;
import java.lang.reflect.Method;
import java.util.List;

import static org.junit.Assert.assertEquals;

public class ParserTest {

  @Ignore
  @Test
  public void verifyThatMethodHasSpecifiedAnnotation() {
    List<Method> methods = Reflections.getMethods(Resource.class);
    Method getMethod = methods.get(0);
    List<Parameter> parameters  = Reflections.getParameters(getMethod);
    Parameter userParam = parameters.get(0);

    assertEquals(String.class, userParam.getType());
    ParamAnnotation annotation = userParam.getAnnotation();
    assertEquals("user", annotation.getField("value"));
  }

  public static class Resource {

    public void get(@QueryParam("user") String user) {

    }
  }
}
