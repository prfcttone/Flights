package ua.danit.application.dao;


import ua.danit.application.model.Flight;
import ua.danit.framework.utils.EntityUtils;


public class FlightDao {

  public Iterable<Flight> getTopFlight(int limit) {
    String sql = "select ID as id, `DEPARTURE_CITY` as `from`, " +
        " `DESTINATION_CITY` as `to`," +
        "`DEPARTURE_TIME` as `departureTime`," +
        "`ARRIVAL_TIME` as `arrivalTime`" +
        "from FLIGHTS";
    return EntityUtils.nativeQuery(sql, Flight.class);
  }


  public Iterable<Flight> searchFlights(String str) {
    if (str != null) {
      String sql = "select ID as id, `DEPARTURE_CITY` as `from`, " +
          " `DESTINATION_CITY` as `to`," +
          "`DEPARTURE_TIME` as `departureTime`," +
          "`ARRIVAL_TIME` as `arrivalTime`" +
          "from FLIGHTS " +
          "where `DEPARTURE_CITY` like'" + str + "%' " +
          "or `DESTINATION_CITY` like '" + str + "%'";
      return EntityUtils.nativeQuery(sql, Flight.class);
    }
    return null;
  }

}
